<?php
#ä
namespace Enduron\Core;

class EnduronCore
{

	public const KERNEL_MODE_HTTP = 1;
	public const KERNEL_MODE_CMD = 2;

	public function __construct(string $dir)
	{
		require_once __DIR__ . '/Helper/helper.php';

		define('ENDURON_START', microtime(true));
		define('ROOT_PATH', realpath($dir) . '/');

		$this->_parseEnv();
		$this->_parseConfig();
	}

	private function _parseEnv()
	{
		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ //
		// load .env
		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ //
		if( !file_exists(ROOT_PATH . '/.env') )
			exit("please set .env file in root path [" . ROOT_PATH . "]");

		$lines = explode("\n", file_get_contents( ROOT_PATH . '/.env') ?? '');
		foreach( $lines as $line )
		{
			$line = str_replace("\r", '', $line );
			$line = str_replace("\"", '', $line );
			if( empty($line) OR preg_match('/^[\s\t]*#/', $line) )
				continue;

			putenv($line);
			$vars = explode("=", $line );
			$value = $vars[1];

			if( is_numeric($value) )
				$value = (int) $value;
			if( strtolower($value) == 'true')
				$value = true;
			if( strtolower($value) == 'false')
				$value = false;
			if( strtolower($value) == 'null')
				$value = null;

			$_ENV[$vars[0]] = $value;
		}
	}

	private function _parseConfig()
	{
		if( !file_exists(ROOT_PATH . '/application/config/app.php') )
			exit("no config file for app. please set [application/config/app.php]");

		$app = include_once ROOT_PATH . '/application/config/app.php';
	}

	public function run(?int $kernel_mode = self::KERNEL_MODE_HTTP)
	{
		if( $kernel_mode == self::KERNEL_MODE_HTTP )
		{
			$myHttpKernel = new \Enduron\Core\HttpKernel();
			$myHttpKernel->doRequest();

			if( getenv('APP_ENV') == 'development' )
			{
				header('Enduron-Memory-Usage: ' . round(memory_get_usage() / 1024, 3) . ' kb');
				header('Enduron-Runtime: ' . round(microtime(true) - ENDURON_START, 4) . ' sec');
				header('Enduron-DB-Queries: ' . round($_ENV['db_timer'], 4 ). ' ms');
			}

			exit;
		}

		return (new \Enduron\Core\CmdKernel())->run();
	}
}