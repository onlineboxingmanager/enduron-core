<?php
#ä
namespace Enduron\Core\DBAL;

class SQLLimit
{

    private int $limit;
    private int $offset;

    public function __construct(?int $limit = 30, ?int $offset = 0)
    {
        $this->limit = $limit;
        $this->offset = $offset;
    }

    public function getLimit(): int
    {
        return $this->limit;
    }

    public function getOffset(): int
    {
        return $this->offset;
    }

}