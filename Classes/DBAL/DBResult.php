<?php
#ä
namespace Enduron\Core\DBAL;

use PDOStatement;
use Enduron\Core\Utilities\UUID;

class DBResult
{

	protected ?PDOStatement $stmt = null;
	protected bool $is_next = false;
	protected mixed $row = [];

	public function __construct(PDOStatement $statement)
	{
		$this->stmt = $statement;
	}

	public function getUuid( string $column ): UUID|null
	{
		if( is_null($this->row[$column]) )
			return new UUID();

		return new UUID($this->row[$column]);
	}

	public function next(): bool
	{
		$this->row = $this->fetch();
		return !empty($this->row);
	}

	public function getString($column): string|null
	{
		if( is_null($this->row[$column]) )
			return null;

		return (string) $this->row[$column];
	}

	public function getInt($column): int|null
	{
		if( is_null($this->row[$column]) )
			return null;

		return (int) $this->row[$column];
	}

	public function getFloat($column): float|null
	{
		if( is_null($this->row[$column]) )
			return null;

		return (float) $this->row[$column];
	}

	public function getRow(): array
	{
		return (array) $this->row;
	}

	public function fetch()
	{
		return $this->stmt->fetch(\PDO::FETCH_ASSOC);
	}

	public function clear(): void
	{
		$this->stmt = null;
	}

}