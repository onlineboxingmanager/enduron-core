<?php
#ä
namespace Enduron\Core\DBAL;

use Exception;
use PDO;

/**
 * Class to create and store a connection to the database
 */
abstract class DBConnection
{

    protected static ?PDO $conn = null;

    public static function getConnection(): PDO
    {
        if ( is_null(self::$conn) )
        {
            $db_host = getenv('DB_HOST');
            $db_port = getenv('DB_PORT') ?? 3306;
            $db_dbname = getenv('DB_DATABASE');
            self::$conn = new PDO("mysql:host=$db_host;port=$db_port;dbname=$db_dbname", getenv('DB_USERNAME'), getenv('DB_PASSWORD'), [
                PDO::ATTR_DEFAULT_FETCH_MODE =>	PDO::FETCH_ASSOC, // fetch modus
                PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8', // client kommuniziert mit utf8
                PDO::ATTR_PERSISTENT => false, // keine persistente Verbindungen zulassen
            ]);

            // error modus
            self::$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

			// set sql_mode to none
            self::$conn->query("SET SESSION sql_mode=''")->execute();

            if (is_null(self::$conn))
                throw new Exception ('no connection!');

            //self::$conn->setAutoCommit(false);
            self::$conn->beginTransaction(); // turn off auto-commit mode

            /* Set the desired charset after establishing a connection */
            //self::$conn->set_charset('utf8mb4');
            //self::$conn->set_charset('utf8');
            //BaseDAO::genericQuery('SET NAMES \'utf8\'', array());
        }

        return self::$conn;
    }

    public static function disconnect(): void
    {
        //echo 'disconnect<hr>';
        //self::getConnection()->commit(); // transaction wird abgeschlossen und transaktions-modus geht zurück auf auto-commit
        //self::getConnection()->close(); // verbindung schliessen

        self::getConnection()->commit();
        self::$conn = null;
    }

    public static function commit(): void
    {
        if( self::getConnection()->inTransaction() )
            self::getConnection()->commit(); // transaction wird abgeschlossen und transaktions-modus geht zurück auf auto-commit

        //self::getConnection()->begin(); // transaktions-modus auf DEFAULT umstellen - OCI8 "0" - transaction wartet auf commit/rollback
        self::getConnection()->beginTransaction();
    }

    public static function rollback(): void
    {
        if( self::getConnection()->inTransaction() )
            self::getConnection()->rollback();// transaction abbrechen und transaktions-modus geht zurück auf auto-commit

        //self::getConnection()->begin(); // transaktions-modus auf DEFAULT umstellen - OCI8 "0" - transaction wartet auf commit/rollback
        self::getConnection()->beginTransaction();
    }

    public static function genericQuery(string $sql, ?array $psWhereParams, ?SQLLimit $limitation = null): DBResult
    {
        $timer = microtime(true);
        $conn = self::getConnection();

        $has_limit = preg_match('/LIMIT /ui', $sql);
        if( !$has_limit AND $limitation )
        {
            $sql .= ' #limit #offset';
            $sql = str_replace('#limit', ' LIMIT ' . $limitation->getLimit() ?? 30, $sql );
            $sql = str_replace('#offset', ' OFFSET ' . $limitation->getOffset() ?? 0, $sql );
        }

        // prepare
        $stmt = $conn->prepare($sql);

        //echo $sql;

        // execute query
        try {
            $result = $stmt->execute($psWhereParams);
        }
        catch ( Exception $e)
        {
            echo "<hr><pre>";
            var_dump($e);
            echo "<hr>";
            echo $stmt->queryString;
            echo "<hr>";
            var_dump($psWhereParams);
            echo "</pre>";
        }

        $taken = round(microtime(true) - $timer, 4);

        @$_ENV['SQL_COUNT']++;
        @$_ENV['SQL_TIME'] += $taken;

        if( !empty($_SERVER['argv']) AND defined('GLOBAL_SLOW_QUERY_LOG') AND $taken > 0.1 )
        {
            $text = "\nslow query detected. usage: $taken s \n$sql\n";
            echo $text;
            file_put_contents(GLOBAL_SLOW_QUERY_LOG, $text, FILE_APPEND);
        }

        if ( isset($_GET['debugmode']) )
        {
            echo'<div class="position-relative badge badge-info text-sm" style="z-index: 999999" data-toggle="tooltip" title="' . substr($sql, 0, 250) . '">
			 SQL ' . $taken . 's' . '
			 </div>';
            echo "<script>console.log('".json_encode($sql)."');</script>";
            echo "<script>console.log('".json_encode($psWhereParams)."');</script>";
            echo "<script>console.log('sql time: $taken');</script>";
            echo "<script>console.log('memory ".( memory_get_usage() / 1024 / 1000 )." mb');</script>";
        }

        $result = new DBResult($stmt);
        $stmt = null; // free memory

        return $result;
    }

}