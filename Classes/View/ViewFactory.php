<?php
#ä
namespace Enduron\Core\View;

use Exception;

class ViewFactory
{
	protected string $view_modus = 'full';
	protected string $language = 'de'; // default
	protected array $assign_list = array();

	protected string $layout = 'empty';
	protected string $template = 'start/index';

	protected array $renderlist = [];

	/**
	 * own parse function
	 *
	 * @throws Exception
	 */
	private function _parse(): void
	{
		$template = GLOBAL_VIEWS . $this->template . '.blade.php';
		if(!file_exists($template))
			throw new Exception('View not exists: '.$template);

		$template_change_time = filemtime($template);

		$cachefile = GLOBAL_CACHE . 'views/' . $this->_hash() . '.php';
		$cache_file_exists = file_exists($cachefile);

		// TODO: gescheite logik einbauen. sonst mega hohe IO Load
		if( ( getenv('APP_ENV') ?? 'development' ) === 'production' AND $cache_file_exists )
		{
			$cache_create_time = filemtime($cachefile);
			$file_is_newer_than_cachefile = ($template_change_time > $cache_create_time);

			// skip wenn cache file noch neuer als template ist
			if( !$file_is_newer_than_cachefile AND !isset($_GET['force']) ) {
				return;
			}
		}

		#if( file_exists($cachefile) AND time() - filemtime($cachefile) < 86400 )
		#	return false;

		$myView = file_get_contents( $template );

		// replace pattern php code block
		$myView = str_replace('@php', '<?php', $myView);
		$myView = str_replace('@endphp', '?>', $myView);

		// replace if
		$myView = preg_replace('/@if(.*)/i', '<?php if $1 : ?>', $myView);
		$myView = preg_replace('/@elseif(.*)/i', '<?php elseif $1 : ?>', $myView);
		$myView = preg_replace('/@else[^i]/i', '<?php else: ?>', $myView);
		$myView = str_replace('@endif', '<?php endif; ?>', $myView);

		$myView = preg_replace('/@continue/i', '<?php continue; ?>', $myView);
		$myView = preg_replace('/@break/i', '<?php break; ?>', $myView);

		// replace foreach
		$myView = preg_replace('/@foreach(.*)/i', '<?php foreach $1 : ?>', $myView);
		$myView = preg_replace('/@endforeach/i', '<?php endforeach; ?>', $myView);

		$myView = preg_replace('/@for(.*)/i', '<?php for $1 : ?>', $myView);
		$myView = preg_replace('/@endfor/i', '<?php endfor; ?>', $myView);

		// replace switch
		$myView = preg_replace('/@switch(.*)/i', '<?php switch $1: ?>', $myView);
		$myView = preg_replace('/@endswitch/i', '<?php endswitch; ?>', $myView);
		$myView = preg_replace('/@break/i', '<?php break; ?>', $myView);
		$myView = preg_replace('/@default/i', '<?php default: ?>', $myView);
		$myView = preg_replace('/.*@case(.*)/i', '<?php case $1: ?>', $myView);

		// replace for
		$myView = preg_replace('/@for(.*)/i', '<?php for $1 : ?>', $myView);
		$myView = preg_replace('/@endfor/i', '<?php endfor; ?>', $myView);

		// comments
		$myView = preg_replace('/\{\{\-\-/i', '<?php /*', $myView);
		$myView = preg_replace('/\-\-\}\}/i', '*/ ?>', $myView);

		// replace vars
		$myView = preg_replace('/\{\{/i', '<?php echo ', $myView);
		$myView = preg_replace('/\}\}/i', '?>', $myView);

		// push
		$myView = preg_replace('/@push\(([a-z0-9\']+)\)/uis', '<?php $push_type = $1; ob_start(); ?>', $myView);
		$myView = str_replace('@endpush', '<?php $_ENV[\'stack\'][$push_type][md5(ob_get_contents())] = ob_get_clean(); ?>', $myView);

		// stacks
		$myView = preg_replace('/@stack\(([a-z0-9\']+)\)/ui', '<?php if( !empty($_ENV[\'stack\'][$1]) ) { $str = implode( "\r", $_ENV[\'stack\'][$1]); eval (\'?>\' . $str); }  ?>', $myView);

		// replace foreach
		$regexp_foreach = '/({foreach [a-z0-9]+ as [a-z0-9]+ \=\> [a-z0-9]+}.*{[a-z0-9\(\)\.]+}+.*{\/foreach})+/is';
		$hits = array();
		preg_match($regexp_foreach, $myView, $hits);
		foreach( $hits as $html )
		{
			$foreach_block = $html;
			$foreach_block = preg_replace('/\{foreach ([a-z0-9]+) as ([a-z0-9]+) \=\> ([a-z0-9]+)\}/i', '<?php if(!empty($data[\'\1\'])) foreach( $data[\'\1\'] as $\2 => $\3 ){ ?>', $foreach_block);
			$foreach_block = preg_replace('/\{\/foreach\}/', '<?php } ?>', $foreach_block);
			$foreach_block = preg_replace('/{(\w+) ([\=\<\>\!]+) (.+) \? ([\'\w.*]+) \: ([\'\w]+)}/i', '<?php echo ( $\1 \2 \3 ? \4 : \5 )?>', $foreach_block);
			$foreach_block = preg_replace('/{(\w+)\.([\w]+) ([\=\<\>\!]+) (.+) \? ([\'\w]+) \: ([\'\w]+)}/i', '<?php echo ( $\1->get\2() \3 \4 ? \5 : \6 )?>', $foreach_block);
			/*$foreach_block = preg_replace('/{(\w+)\.([\w]+) ([\=\<\>\!]+) (\w+)\.(\w+) \? ([\'\w]+) \: ([\'\w]+)}/i', '<?=( $\1->get\2() \3 $\4->get\5() ? \6 : \7 )?>', $foreach_block);*/
			$foreach_block = preg_replace('/\{([a-z0-9]+)\}/i', '<?=$\1?>', $foreach_block);
			$foreach_block = preg_replace('/\{([a-z0-9]+\()?([\w]+)\.([\w]+)(\))?\}/i', '<?php echo \1$\2->get'.'\3'.'()\4?>', $foreach_block);
			$foreach_block = preg_replace('/{(\w+)\.(\w+) ([\=\<\>\!]+) (\w+) \? (\w+) \: (\w+)}/i', '<?php echo ( $\1->get'.'\2'.'() \3 \4 ? \5 : \6 )?>', $foreach_block);

			// if
			$foreach_block = preg_replace('/\{if ([\!a-z0-9]+\()?([a-z0-9]+)(\))?\}/i', '<?php if ( \1$data\2()\3 ){ ?>', $foreach_block);

			//
			$foreach_block = preg_replace('/ ([\w]+)\.([\w]+) /i', ' $data[\'\1\']->get\2() ', $foreach_block);

			$myView = preg_replace($regexp_foreach, $foreach_block, $myView, 1);
		}

		// if statement
		$myView = preg_replace('/\{if ([\!a-z0-9]+\()?([a-z0-9]+)(\))?\}/i', '<?php if ( \1$data[\'\2\']\3 ){ ?>', $myView);

		/*$myView = preg_replace('/\{if ([\!a-z0-9\(\)]+)\}/i', '<?php if ( $\1){ ?>', $myView);*/
		$myView = preg_replace('/\{\/if}/i', '<?php } ?>', $myView);
		$myView = preg_replace('/\{else}/i', '<?php } else { ?>', $myView);

		// vars
		foreach($this->assign_list as $placeholder => $assign)
		{
			if( is_array($assign) OR is_object($assign) )
				continue;

			$myView = preg_replace('/\{'.$placeholder.'\}/i', '<?php echo \''.$assign.'\';?>', $myView);
		}
		$myView = preg_replace('/\{([\w]+)\}/i', '', $myView); // raus mit dem rest
		$myView = preg_replace('/\{([\w]+)\.([\w]+)}/i', '<?php echo $data[\'\1\']->get'.'\2'.'()?>', $myView);

		// save parsed template
		$dir = dirname($cachefile);
		if(!is_dir($dir))
			mkdir($dir, 0777, true);
		file_put_contents( $cachefile, $myView);
		chmod($cachefile, 0775);
		//error_log(":: Write Cachefile: $cachefile\n", 0);
	}

	// TODO: hier muss noch caching rein
	private function _read(): bool|string
	{
		try
		{
			// get ouput from cache file
			if( ( getenv('APP_ENV') ?? 'development' ) === 'production' AND isset($this->renderlist[$this->template]) )
			{
				$static_cache_name = $this->renderlist[$this->template];
				$cache_file = GLOBAL_CACHE . 'output/' . $static_cache_name;
				if( !isset($_GET['force']) AND $static_cache_name AND file_exists($cache_file) )
					return file_get_contents($cache_file);
			}

			$this->_parse();

			ob_start();

			// assign vars
			foreach( $this->assign_list as $key => $value )
				$$key = $value;

			// include template
			include GLOBAL_CACHE . 'views/' . $this->_hash(). '.php';

			$output = ob_get_clean();

			// write output to cache file
			if( ( getenv('APP_ENV') ?? 'development' ) === 'production' AND isset($this->renderlist[$this->template]) )
			{
				$cache_file = GLOBAL_CACHE . 'output/' . $this->renderlist[$this->template];
				if( !is_dir(dirname($cache_file)) )
					mkdir(dirname($cache_file), 0777,true);

				file_put_contents($cache_file, $output);
				chmod($cache_file, 0755);
				//error_log(':: WRITE OUPUT: ' . $cache_file, 0);
			}

			// return ob cache and clean
			return $output;
		}
		catch ( Exception $e )
		{
			//var_dump($e);
			throw $e;
		}
	}

	private function _hash(): string
	{
		//return md5( serialize($this->assign_list) . $this->view . md5(file_get_contents(GLOBAL_VIEWS . $this->view . '.php')) );
		return str_replace(['/'], '-', $this->template) . '-' . md5( serialize($this->template) );
	}

	public function assign(string $placeholder, mixed $value): self
	{
		$this->assign_list[$placeholder] = $value;
		return $this;
	}

	public function setLayout(string $ayout): void
	{
		$this->layout = $ayout;
	}

	public function getLayout(): string
	{
		return $this->layout;
	}

	protected function _render($return = false): Exception|bool|string
	{
		$output = $this->_read();

		$js = [];
		$css = [];

		if( !empty($_ENV['stack']['js']) )
			$js =  $_ENV['stack']['js'];
		if( !empty($_ENV['stack']['css']) )
			$css =  $_ENV['stack']['css'];

		if( $return AND \isAjax() )
			return $output . implode("\n", $js) . implode("\n", $css); // TODO: mit empty.blade.php view lösen
		elseif( $return )
			return $output;

		return $this->view($this->layout, ['content' => $output], true, null);
	}

	public function output( string $templatename, ?array $data = [], ?bool $return_as_string = false, ?string $layout = null )
	{
		return $this->view( $templatename, $data, $return_as_string, $layout);
	}

	public function view( string $templatename, ?array $data = [], ?bool $return_as_string = false, ?string $layout = null, ?string $static_cache_name = null ): bool|Exception|string
	{
		$this->renderlist[$templatename] = $static_cache_name; // add template to render list

		$this->template = $templatename;

		if( $layout )
			$this->layout = $layout;

		// assign data
		foreach( $data as $key => $value )
		{
			$this->assign_list[$key] = $value;
		}

		return $this->_render($return_as_string);
	}

	public function render( string $templatename, ?array $data = [], ?bool $return_as_string = false, ?string $layout = 'public' ): bool|Exception|string
	{
		return $this->view($templatename, $data, $return_as_string, $layout );
	}

	public function getPost(string $key, mixed $default = null): mixed
	{
		if( isset($_POST[$key]) )
			return $_POST[$key];

		return $default;
	}

	public function getInput(string $key, mixed $default = null): mixed
	{
		if( isset($_GET[$key]) )
			return $_GET[$key];

		return $default;
	}

	public function getRequest(string $key, mixed $default = null): mixed
	{
		if( isset($_REQUEST[$key]) )
			return $_REQUEST[$key];

		return $default;
	}
}

if (! function_exists('view'))
{

	function view( string $templatename, ?array $data = [], ?bool $return_as_string = false, ?string $layout = 'public', ?string $static_cache_name = null ): bool|Exception|string
	{
		if( \isAjax() )
			$return_as_string = true;

		if( $return_as_string === true )
			$layout = 'empty';

		$myView = new ViewFactory();
		return $myView->view($templatename, $data, $return_as_string, $layout, $static_cache_name);
	}
}