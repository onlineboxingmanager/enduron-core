<?php
#ä
namespace Enduron\Core\Utilities;

use Enduron\Core\ORM\BaseObject;
use ReturnTypeWillChange;

/**
 * base list class (collections)
 * @author Mario Kaufmann
 */
class BaseList implements \IteratorAggregate, \Countable
{
	protected array $list;
	protected int $pos = 0;

	public function __construct()
	{
		$this->pos = 0;
		$this->list = [];
	}

	public function getIterator(): \Generator
	{
		yield from $this->list;
	}

	/**
	 * extract current element from list
	 */
	public function extract()
	{
		$object = $this->current();
		$this->remove();
		return $object;
	}

	/**
	 * remove current element
	 */
	public function remove(): void
	{
		unset($this->list[$this->pos]);
		$this->list = array_values($this->list); // keys neu generien
		$this->next();
	}

	public function add(BaseObject $myObject): void
	{
		$this->list[] = $myObject;
	}

	public function rewind(): void
	{
		$this->pos = 0;
	}

	#[ReturnTypeWillChange]
	public function current(): mixed
	{
		return $this->list[$this->pos];
	}

	public function get($pos): mixed
	{
		return $this->list[$pos] ?? null;
	}

	#[ReturnTypeWillChange]
	public function key(): int
	{
		return $this->pos;
	}

	public function pos(): int
	{
		return $this->pos;
	}

	public function next(): mixed
	{
		if ($this->pos == (count($this->list) - 1))
		{
			$this->reset();
			return false;
		}

		return $this->list[++$this->pos];
	}

	public function reset(): bool
	{
		if ($this->count() == 0)
			return false;
		$this->pos = 0;
		return true;
	}

	public function valid(): bool
	{
		return isset($this->list[$this->pos]);
	}

	public function count(): int
	{
		return count($this->list);
	}

	public function shuffle(): self
	{
		shuffle($this->list);
		return $this;
	}

	public function toArray(bool $include_tstamps_and_users = true): array
	{
		$list = [];
		foreach ( $this->list as $item )
		{
			/**
			 * @var BaseObject $item
			 */
			$list[] = $item->toArray($include_tstamps_and_users);
		}

		return $list;
	}

	/**
	 * extract first element from list
	 */
	public function shift()
	{
		return array_shift($this->list);
	}

	/**
	 * extract last element from list
	 */
	public function pop()
	{
		return array_pop($this->list);
	}

	public function reverse()
	{
		$this->list = array_reverse($this->list);
	}

	public function getBySearch(string $column, mixed $value)
	{
		$column = 'get' . $column;
		foreach ($this->list as $pos)
			if ($pos->$column() == $value)
				return $pos;

		return false;
	}

	public function getItemByAttribute(string $attribute, mixed $value)
	{
		foreach ($this->list as $pos)
			if ( $pos->getAttribute($attribute) == $value )
				return $pos;

		return null;
	}

	public function getListByAttribute(string $attribute, mixed $value): BaseList
	{
		$return = new BaseList();
		foreach ($this->list as $pos)
			if ( $pos->getAttribute($attribute) == $value )
				$return->add($pos);

		return $return;
	}

}