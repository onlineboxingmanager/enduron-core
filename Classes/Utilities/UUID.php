<?php
namespace Enduron\Core\Utilities;

class UUID
{
	protected ?string $value = null;

	public function __construct(?string $value = null)
	{
		$this->setValue($value);
	}

	public function getValue(): ?string
	{
		return $this->value;
	}

	public function setValue( ?string $value ): void
	{
		$this->value = $value;
	}

	public static function generate(): string
	{
		// Generiere eine Version 4 UUID
		$data = openssl_random_pseudo_bytes(16);
		$data[6] = chr(ord($data[6]) & 0x0f | 0x40); // Version 4
		$data[8] = chr(ord($data[8]) & 0x3f | 0x80); // Variant RFC 4122

		// Formatieren der UUID
		return vsprintf('%s%s-%s-%s-%s-%s%s%s', str_split(bin2hex($data), 4));
	}

	public static function isUuidString(string $uuid): bool
	{
		return preg_match('/^[0-9a-f]{8}-[0-9a-f]{4}-[0-5][0-9a-f]{3}-[089ab][0-9a-f]{3}-[0-9a-f]{12}$/', $uuid);
	}
}