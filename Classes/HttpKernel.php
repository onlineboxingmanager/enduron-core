<?php
#ä
namespace Enduron\Core;

class HttpKernel
{

	public function __construct()
	{
        // proxy configurations
        if( isset($_SERVER['HTTP_X_REAL_IP']) )
            $_SERVER['REMOTE_ADDR'] = $_SERVER['HTTP_X_REAL_IP'];
        if( isset($_SERVER['HTTP_X_FORWARDED_FOR']) )
            $_SERVER['REMOTE_ADDR'] = $_SERVER['HTTP_X_FORWARDED_FOR'];
        if( isset($_SERVER['HTTP_X_FORWARDED_HOST']) )
            $_SERVER['HTTP_HOST'] = str_replace(array(':', '80', '443'), '', $_SERVER['HTTP_X_FORWARDED_HOST']);
        if( isset($_SERVER['HTTP_X_FORWARDED_PROTO']) AND $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https' )
            $_SERVER['HTTPS'] = true;

		// clear post, get, requests
		$_POST = $this->_cleanPostGetRequest($_POST);
		$_GET = $this->_cleanPostGetRequest($_GET);
		$_REQUEST = $this->_cleanPostGetRequest($_REQUEST);
	}

	public function doRequest(): string
	{
        header("Content-Type: text/html; charset=utf-8");

		$routelist = require_once ROOT_PATH . 'application/config/route.php';

		// default
		$controller = 'start';
		$action = 'index';
		$value = null;

		$uri = rawurldecode(substr($_SERVER['REQUEST_URI'], 1));
		$uri = preg_replace('/\?.*/', '', $uri);

		$uri_parts = explode('/', $uri);
		if( !empty($uri_parts[0]) )
			$controller = $uri_parts[0];
		if( !empty($uri_parts[1]) )
			$action = $uri_parts[1];
		if( !empty($uri_parts[2]) )
			$value = $uri_parts[2];

		unset($uri_parts[0]);
		unset($uri_parts[1]);

		$class = 'Enduron\\HTTP\\Controller\\' . ucwords($controller) . 'Controller';
		$controller = ucwords($controller);

		header("Content-Languge: de");

		// switch locales (required for urls)
		setlocale(LC_TIME, 'de_DE');

		// check route
		if( !empty($uri) )
		{
			foreach( $routelist as $route => $mapping )
			{
				if( preg_match('/'.$route.'/', $uri) OR in_array(strtolower($controller), [strtolower(trim($route)), $route]) )
				{
					$explode = explode('/', $mapping);
					$controller = ucwords($explode[0]);

					$action = 'index';
					if( !empty($explode[1]) )
						$action = $explode[1];

					$class = 'Enduron\\HTTP\\Controller\\' . $controller . 'Controller';

					break;
				}
			}
		}

		if( !class_exists($class) )
		{
			$action = 'error_404';
			$controller = 'Start';
			$class = \Enduron\HTTP\Controller\StartController::class; // default
		}

		$_ENV['controller'] = strtolower($controller);
		$_ENV['action'] = strtolower($action);

		// load controller
		//$path = ROOT_PATH . 'application/controller/' . ucwords($controller) . '.php';
		if( !class_exists($class) )
			exit("please add controller for $class");

		//include_once ROOT_PATH . 'application/controller/' . ucwords($controller) . '.php';

		try
		{
			$myController = new $class('public', strtolower($controller) . '/'. strtolower($action));

			ob_start();

            if ( method_exists($class, $action) )
            {
                $uri_parts = array_values($uri_parts);
                $reflection = new \ReflectionObject($myController);
                $parameters = $reflection->getMethod($action)->getParameters();
                foreach( $parameters as $key => $parameter )
                {
                    $type = $parameter->getType();
                    if (!$type || $type->isBuiltin())
                        continue;

                    if(!class_exists($parameter->getName()))
                        continue;

                    $classname = $parameter->getName();
                    $myClass = new $classname();
                    $myObject = $myClass::find($uri_parts[$key]);
                    if( !$myObject )
                        throw new \Exception('data not found');

                    $uri_parts = $myObject;
                }

                // Call function with arguments
                $output = call_user_func_array(array($myController, $action), $uri_parts);
                echo $output;
            }

			else
			{
				$myController = new \Enduron\HTTP\Controller\StartController();
				$myController->response_404();
			}

			// close transaction - at this point no error was thrown
			if ( class_exists( \Enduron\Core\DBAL\DBConnection::class) )
			{
				\Enduron\Core\DBAL\DBConnection::commit();
				\Enduron\Core\DBAL\DBConnection::disconnect();
			}

			$buffer = ob_get_contents();

			return $buffer;
		}
		catch ( \Exception $e)
		{
            http_response_code(503);
            exception_handler($e);
			dd($e);
		}
	}

	private function _cleanPostGetRequest(array $array): array
	{
		// clean post fields
		foreach ($array as $key => $value)
		{
			if( is_string($value) )
				$array[$key] = trim($value);
			if( is_array($value) )
				$array[$key] = $this->_cleanPostGetRequest($value);
		}

		return $array;
	}
}