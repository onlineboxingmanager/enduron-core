<?php
#ä
namespace Enduron\Core\Controller;

use Enduron\Core\DBAL\DBConnection;
use Enduron\Core\Utilities\BaseList;
use Enduron\Core\View\ViewFactory;

abstract class BaseController
{

	protected ?ViewFactory $view;

	public function __construct()
	{
		// init new layout
		$this->view = new ViewFactory();
	}

	public function getInput(string $name, mixed $fallback = null): mixed
	{
		return ( $_GET[$name] ?? $fallback );
	}

	public function getPost(string $name, mixed $fallback = null): mixed
	{
		return ( $_POST[$name] ?? $fallback );
	}

	public function getRequest(string $name, mixed $fallback = null): mixed
	{
		return ( $_REQUEST[$name] ?? $fallback );
	}

	public function hasPost(): bool
	{
		return !empty($_POST);
	}

	public function setHttpStatus(int $code): void
	{
		header_remove();
		http_response_code($code);
	}

	public function error_404()
	{
		return $this->response_404();
	}

	public function response_404(): void
	{
		http_response_code(404);
		exit;
	}

	/**
	 * redirect temporary (no cacheing)
	 */
	public function redirect(string $path, ?int $response_code = 307): void
	{
		$path = str_replace('://', ':##', $path);
		if( !preg_match('/^http/ui', $path) )
			$path = '/' . $path;
		$path = str_replace('//', '/', $path);
		$path = str_replace(':##', '://', $path);

		$this->setHttpStatus($response_code);
		header('location: ' . $path);

		DBConnection::commit();
		exit();
	}

	/**
	 * temporary rediect
	 */
	public function back(?string $additional = ''): void
	{
		$referer = $_SERVER['HTTP_REFERER'];
		if( isAjax() )
			$referer .= '?ajax=true';

		$referer .= $additional;

		$this->redirect( $referer, 307 );
	}

	public function response(string $templatename, ?array $data = [], ?bool $return_as_string = false, ?string $layout = null, ?string $static_cache_name = null ): bool|Exception|string
	{
		return $this->view->view($templatename, $data, $return_as_string, $layout, $static_cache_name );
	}

	public function responseJson(array $data = []): string
	{
		ksort($data);

		header_remove();
		header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
		header("Cache-Control: post-check=0, pre-check=0", false);
		header("Pragma: no-cache");
		header('Content-Type: application/json; charset=utf-8');

		if( getenv('APP_ENV') == 'development' )
			header('DEV-Server-Memory-Usage: ' . round(memory_get_usage() / 1024 / 1000, 2) . 'mb' );

		return json_encode($data, JSON_PRETTY_PRINT);
	}

	public function responseJsonCollection(BaseList $myList, int $total = 0, int $offset = 0, int $limit = 25 ): string
	{
		header_remove();
		header('Content-Type: application/json; charset=utf-8');

		if( getenv('APP_ENV') == 'development' )
			header('DEV-Server-Memory-Usage: ' . round(memory_get_usage() / 1024 / 1000, 2) . 'mb' );

		$collection = (object) [];
		$collection->total = $total;
		$collection->page = ceil($offset / $limit) + 1;
		$collection->offset = $offset;
		$collection->limit = $limit;
		$collection->collection = $myList->toArray();

		$class_parts = explode('\\', $myList::class);
		$collection->schema = str_replace('List', '', end($class_parts));

		return json_encode($collection);
	}

	abstract public function index();
}