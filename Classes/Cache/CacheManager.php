<?php
#ä
namespace Enduron\Core\Cache;

/**
 * Cache Manager to use cache small amount of frequently used data.
 * @author Mario Kaufmann, bueroparallel GmbH
 */
abstract class CacheManager
{

	protected static bool $use_gzip = true;

	/**
	 * Retrieves APC's Shared Memory Allocation information
	 */
	public static function getSharedMemoryAllocationInformation( $bool = FALSE ): array|false
	{
		return apcu_sma_info($bool);
	}

	/**
	 * Retrieves cached information from APC's data store
	 */
	public static function getCacheInformation( bool $limited = false ): array|false
	{
		return apcu_cache_info($limited);
	}

	/**
	 * Get the cached data.
	 */
	public static function get( string $key ): mixed
	{
		$output = apcu_fetch($key);

		if (self::$use_gzip)
		{
			$output = gzdecode($output);
			$output = unserialize($output);
		}

		return $output;
	}

	public static function exists( string $key ): bool
	{
		return apcu_exists($key);
	}

	/**
	 * Store the data in the cache.
	 */
	public static function set( string $key, mixed $data, int $ttl = 0 ): array|bool
	{
		if( self::$use_gzip )
		{
			$data = serialize($data);
			$data = gzencode($data);
		}

		return apcu_store($key, $data, $ttl);
	}

	/**
	 * Removes a stored variable from the cache.
	 */
	public static function delete( string $key ): bool|array
	{
		return apcu_delete($key);
	}

	/**
	 * Generate a key based on the parameters of the db request.
	 */
	public static function genKey( string $type, array $param = [] ): string
	{
		return md5($type . implode($param));
	}

}