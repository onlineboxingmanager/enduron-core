<?php

namespace Enduron\Core\Image;

abstract class Image
{

    public static function getImage(string $file, string $name = '', string $extension = 'jpg', ?int $width = null, ?int $height = null, bool $crop = false, bool $rewrite = false): bool|string
    {
        $file_org = $_SERVER['DOCUMENT_ROOT'] . $file;
        $file = str_replace( $_SERVER['DOCUMENT_ROOT'], '', $file);
        //$file = str_replace( '/..', '..', $file);
        $path = substr($file, 0, strrpos($file, '/') + 1 );

        $public_path = self::_getPublicPath($path, $name, $width, $height, $extension);
        $full_cache_path = self::_getCachePath($path, $name, $width, $height, $extension);

        // skip if no rewrite is called
        if( $rewrite == false AND file_exists($full_cache_path) )
            return $public_path;

        $local_path = $file_org;
        if( !file_exists($local_path) )
            return false;

        // load an image
        $myImage = new Imagick( $file_org );

        // get the current image dimensions
        $geo = @$myImage->getImageGeometry();

        if( !$geo)
            return false;

        if( !$width )
            $width = $geo['width'];

        // new height
        if( !$height AND $width AND $width <= $geo['width'] )
            $height = (int) round(( $width / $geo['width'] ) * $geo['height']);

        if( !$height AND $width AND $width >= $geo['width'] )
            $height = (int) round(( $geo['width'] /  $width ) * $geo['height']);

        if( $crop )
        {
            // crop the image
            if( ($geo['width']/$width) < ($geo['height']/$height) )
            {
                $myImage->cropImage($geo['width'], floor($height*$geo['width']/$width), 0, (($geo['height']-($height*$geo['width']/$width))/2));
            }

            else
            {
                $myImage->cropImage(ceil($width*$geo['height']/$height), $geo['height'], (($geo['width']-($width*$geo['height']/$height))/2), 0);
            }
        }

        // thumbnail the image
        $myImage->ThumbnailImage($width, $height,true);

        // format
        $myImage->setImageFormat($extension);

        // compression
        if( $extension == 'jpg' )
        {
            $profiles = $myImage->getImageProfiles("icc", true);

            $myImage->setImageCompression(imagick::COMPRESSION_JPEG);
            #$myImage->setImageCompressionQuality(85);
            $myImage->stripImage();

            if(!empty($profiles))
                $myImage->profileImage('icc', $profiles['icc']);

            $myImage->setInterlaceScheme(Imagick::INTERLACE_JPEG);
            $myImage->setColorspace(Imagick::COLORSPACE_SRGB);
        }

        // format
        $myImage->setImageFormat($extension);

        $myImage->writeImage($full_cache_path);

        return $public_path;
    }

    /**
     * @param $path
     * @param $width
     * @param $height
     * @param $extension
     * @return string
     */
    private static function _getCachePath($path, $name, $width, $height, $extension)
    {
        // generate fullname
        $cachepath = [];
        $cachepath[] = $_SERVER['DOCUMENT_ROOT'];
        $cachepath[] = $path;
        if( !empty($name) )
            $cachepath[] = $name;
        $cachepath[] = '_';
        if( $width )
            $cachepath[] = $width;
        if( $width AND $height )
            $cachepath[] = 'x';
        if( $height )
            $cachepath[] = $height;
        $cachepath[] = '.' . $extension;

        return implode('', $cachepath);
    }

    private static function _getPublicPath($path, $name, $width, $height, $extension)
    {
        $path = str_replace('../', '', $path);
        //$path = str_replace('media/', '', $path);

        // generate fullname
        $cachepath = [];
        //$cachepath[] = 'https://cdn.example.com/';
        $cachepath[] = '/';

        $cachepath[] = $path;
        if( !empty($name) )
            $cachepath[] = $name;
        $cachepath[] = '_';
        if( $width )
            $cachepath[] = $width;
        if( $width AND $height )
            $cachepath[] = 'x';
        if( $height )
            $cachepath[] = $height;
        $cachepath[] = '.' . $extension;

        if( $_ENV['domain']->getDomaintypeid() != 1 )
            $cachepath[] = '?i='.microtime(true);

        return str_replace('//', '/', implode('', $cachepath));
    }

}