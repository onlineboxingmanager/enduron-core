<?php
#ä
namespace Enduron\Core\ORM;

use Enduron\Core\DBAL\DBConnection;
use Enduron\Core\DBAL\DBResult;
use Enduron\Core\DBAL\SQLLimit;
use Enduron\Core\Utilities\UUID;

/**
 * DAO base class
 * @author Mario Kaufmann
 */
abstract class BaseDAO
{

    public static function genericQuery(string $sql, ?array $psWhereParams, SQLLimit $limitation = null): DBResult
    {
        return DBConnection::genericQuery($sql, $psWhereParams, $limitation);
    }

    public const TABLENAME = '';
    protected static array $primarykeys = [];
    protected static array $properties = [];
    protected static array $columns = [];

    public static function getPrimaryKeys(): array
    {
        return static::class::$primarykeys;
    }

    public static function getColumns(): array
    {
        return static::class::$columns;
    }

    public static function getProperties(): array
    {
        return static::class::$properties;
    }

    public static function getTablename(): string
    {
        return static::TABLENAME;
    }

    public static function store(?BaseObject $myObject): int|bool
    {
        if ( $myObject->_getIsNew() AND !$myObject->_getIsDeleted())
            return self::insert($myObject);
        elseif( count($myObject->_getModified()) > 0 )
            self::update($myObject);

        return true;
    }

	private static function insert(BaseObject $object): int
	{
		$parameters = [];
		$inserts = [];
		$values = [];

		foreach( self::getProperties() as $key => $property )
		{
			// skip primary keys for inserts
			if( isset(self::getPrimaryKeys()[$key]) AND $property['is_autoincrement'] === true )
				continue;

			// skip triggers & tstamps
			if( in_array($key, ['tstamp_created', 'tstamp_modified', 'tstamp_deleted', 'userid_modified', 'userid_deleted']) )
				continue;

			$value = $object->getAttribute($key);
			$is_primarykey = isset(self::getPrimaryKeys()[$key]);

			if( $is_primarykey AND $property['typename'] === 'BINARY' AND is_null($value) )
				$value = new UUID();

			// set current user
			if( !empty($_ENV['user']) AND $key == 'userid_created' )
				$value = $_ENV['user']->getUid();

			$inserts[] = $key;

			if( in_array($property['typename'], ['DATE', 'TIMESTAMP']) AND !is_null($value) )
				$parameters[$key] = 'FROM_UNIXTIME(?)';

			elseif( $value instanceof UUID )
			{
				if( empty($value?->getValue()) AND $is_primarykey )
				{
					$parameters[$key] = 'UUID_TO_BIN(uuid())';
					continue;
				}

				else
				{
					$parameters[$key] = 'UUID_TO_BIN(?)';
					$values[] = $value->getValue();
				}

				continue; // skip rest
			}

			else
				$parameters[$key] = '?';

			$values[] = $value;
		}

		$sql = 'INSERT INTO '.self::getTablename().' (' . implode(', ', $inserts) . ') VALUES ( ' . implode(', ', $parameters) . ')';

		try
		{
			$conn = DBConnection::getConnection();
			$stmt = $conn->prepare($sql);

			// execute and get last inserted id
			$stmt->execute($values);
		}
		catch ( Exception $e)
		{
			//echo $e->getMessage();
			//echo str_replace(array_fill(0, count($values), "?"), $values, $sql);
			#echo $sql;
			#var_dump($values);
			#var_dump($e);
			#die();

			// ....
			throw $e;
		}

		return $conn->lastInsertId();
    }

	private static function update(BaseObject $object): void
	{
		$fields = [];
		$values = [];

		foreach( self::getProperties() as $key => $property )
		{
			// skip non-modified attributes
			if( !$object->_getIsModified($key) OR self::getPrimaryKeys()[$key] ?? false )
				continue;

			$value = $object->getAttribute($key);

			// set current user
			if( !empty($_ENV['user']) AND $key === 'userid_modified' AND $object->_getIsModified('userid_modified') )
				$value = $_ENV['user']->getUid();

			$has_value = true;
			$parameter = '?';
			if( in_array($property['typename'], ['DATE', 'TIMESTAMP']) AND $object->_getIsModified($key) )
			{
				$parameter = 'FROM_UNIXTIME(?)';
				if( in_array($key, ['tstamp_modified', 'tstamp_deleted']) )
				{
					$parameter = 'NOW()';
					$has_value = false;
				}
			}

			if( in_array($property['typename'], ['BINARY']) AND $value?->getValue() )
				$parameter = 'UUID_TO_BIN(?)';

			if( in_array($property['typename'], ['BINARY']) AND empty($value) AND $property['is_nullable'] === false )
			{
				$parameter = 'UUID_TO_BIN(uuid())';
				$has_value = false;
			}

			$fields[] = self::getTablename() . '.' . $key . ' = ' . $parameter;
			if( $has_value )
				$values[] = $value;
		}

		// add tstamp modified
		$fields[] = self::getTablename() . '.tstamp_modified = NOW()';
		if( $object->_getIsModified('tstamp_deleted') )
			$fields[] = self::getTablename() . '.tstamp_deleted = NOW()';

		// primary keys
		$primary_wheres = [];
		foreach ( self::getPrimaryKeys() as $key => $function )
		{
			if( self::getProperties()[$key]['typename'] === 'BINARY' )
			{
				$primary_wheres[] = self::getTablename() . '.' . $key . ' = UUID_TO_BIN(?)';
				$value = $object->getAttribute($key)->getValue();
			}

			else
			{
				$primary_wheres[] = self::getTablename() . '.' . $key . ' = ?';
				$value = $object->getAttribute($key);
			}

			$values[] = $value;
		}

		$sql = 'UPDATE '.self::getTablename().' SET ' . implode(', ', $fields) . '
		WHERE (' . implode( ' AND ', $primary_wheres ) . ')';

		$conn = DBConnection::getConnection();
		$stmt = $conn->prepare($sql );

		#var_dump($sql);
		#var_dump($values);

		/*
		echo "<pre>";
		var_dump($primary_wheres);
		print_r($fields);
		print_r($values);
		die();
		*/

		foreach ( $values as $key => $value )
			if( $value instanceof UUID )
				$values[$key] = $value?->getValue();

		// execute and get last inserted id
		try
		{
			$stmt->execute($values);
		}
		catch (Exception $e)
		{
			throw $e;
		}

	}

}