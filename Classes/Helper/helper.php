<?php
#ä

if (! function_exists('isCurrentUrl'))
{
	function isCurrentUrl($url): bool
	{
		if( preg_match('/\/'.$url.'$/ui',$_SERVER['REQUEST_URI']) OR preg_match('/\/'.$url.'\?/ui',$_SERVER['REQUEST_URI'])  )
			return true;

		return false;
	}
}

if (! function_exists('isActive'))
{
	function isActive($url): bool
	{
		return isCurrentUrl($url) ? 'active' : '';
	}
}

if (! function_exists('isValidEmail'))
{
	function isValidEmail($email): bool
	{
		$domain = substr($email, strpos($email, '@') + 1);
		$domain = gethostbyname($domain);
		return preg_match('/^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$/', $domain);
	}
}

if (! function_exists('dd'))
{
	function dd($data)
	{
		ob_end_clean();
		var_dump($data);
		exit;
	}
}

if (! function_exists('isAjax'))
{
	function isAjax(): bool
	{

		$is_ajax_header = (
			!empty($_SERVER['HTTP_X_REQUESTED_WITH'])
			AND strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'
		);

		$is_ajax_param = ( isset($_REQUEST['ajax']) OR isset($_REQUEST['is_ajax']) );

		if ( $is_ajax_header OR $is_ajax_param )
			return true;

		return false;
	}
}

if (! function_exists('trans'))
{
	function trans(string $placeholder, bool $replace = false): string
	{
		return getTrans($placeholder, $replace);
	}
}

if (! function_exists('getTrans'))
{
	function getTrans(string $placeholder, bool $replace = false): string
	{
		// load translations
		if (empty($_ENV['translation']) )
		{
			// include global defaults by system
			$system_translations = include GLOBAL_DOCUMENT_ROOT . '/../../languages/de.php';
			foreach( $system_translations as $key => $translation )
				$_ENV['translation'][strtoupper($key)] = $translation;
		}

		if ( isset($_ENV['translation'][strtoupper($placeholder)]) )
			return $_ENV['translation'][strtoupper($placeholder)];

		if( $replace )
			return $replace;

		return strtoupper('LANG_' . $placeholder);
	}
}

/**
 * translate russion (cyrillic) to lat
 */
if (! function_exists('ru2lat'))
{
	function ru2lat(string $str): string
	{
		$tr = array(
			"А"=>"a", "Б"=>"b", "В"=>"v", "Г"=>"g", "Д"=>"d",
			"Е"=>"e", "Ё"=>"yo", "Ж"=>"zh", "З"=>"z", "И"=>"i",
			"Й"=>"j", "К"=>"k", "Л"=>"l", "М"=>"m", "Н"=>"n",
			"О"=>"o", "П"=>"p", "Р"=>"r", "С"=>"s", "Т"=>"t",
			"У"=>"u", "Ф"=>"f", "Х"=>"kh", "Ц"=>"ts", "Ч"=>"ch",
			"Ш"=>"sh", "Щ"=>"sch", "Ъ"=>"", "Ы"=>"y", "Ь"=>"",
			"Э"=>"e", "Ю"=>"yu", "Я"=>"ya", "а"=>"a", "б"=>"b",
			"в"=>"v", "г"=>"g", "д"=>"d", "е"=>"e", "ё"=>"yo",
			"ж"=>"zh", "з"=>"z", "и"=>"i", "й"=>"j", "к"=>"k",
			"л"=>"l", "м"=>"m", "н"=>"n", "о"=>"o", "п"=>"p",
			"р"=>"r", "с"=>"s", "т"=>"t", "у"=>"u", "ф"=>"f",
			"х"=>"kh", "ц"=>"ts", "ч"=>"ch", "ш"=>"sh", "щ"=>"sch",
			"ъ"=>"", "ы"=>"y", "ь"=>"", "э"=>"e", "ю"=>"yu",
			"я"=>"ya", " "=>"-", "."=>"", ","=>"", "/"=>"-",
			":"=>"", ";"=>"","—"=>"", "–"=>"-"
		);
		return strtr($str, $tr);
	}
}

if (! function_exists('transliteText'))
{
	function transliteText(string $text, string $language, string $country)
	{
		if( strtolower($language) == 'ru' )
			return ru2lat($text);

		$locale_old = setlocale(LC_CTYPE, 0);

		// switch to domain country (required)
		setlocale(LC_CTYPE, strtolower($language). '_' . strtoupper($country) . '.UTF-8');

		if( strtolower($language) == 'de' )
		{
			$text = str_replace(['ö','ä','ü','ß'], ['o','a','u','s'], strtolower($text));
		}

		// translate to ascii cahrs
		$text = iconv("UTF-8", "ASCII//TRANSLIT", $text);

		// switch back locale settings
		setlocale(LC_CTYPE, $locale_old);

		return $text;
	}
}

if (! function_exists('seourl'))
{
	function seourl($url)
	{
		$url = transliteText($url, $_ENV['language'] ?? 'de', $_ENV['country'] ?? 'DE');

		// replace other chars with "-"
		$url = preg_replace('/[^a-z0-9\_\-\/]+/ui', '-', $url);

		// trim last chars
		$url = preg_replace('/[^a-z0-9\/]$/ui', '', $url);

		// trim first chars
		$url = preg_replace('/^[^a-z0-9\/]/ui', '', $url);

		// all to lower
		$url = strtolower($url);

		$url = preg_replace('/[\-]+/', '-', $url);

		return $url;
	}
}

if (! function_exists('dump'))
{
	function dump($data, $label = '', $return = false)
	{
		$debug = debug_backtrace();
		$callingFile = $debug[0]['file'];
		$callingFileLine = $debug[0]['line'];

		ob_start();
		var_dump($data);
		$c = ob_get_contents();
		ob_end_clean();

		$c = preg_replace("/\r\n|\r/", "\n", $c);
		$c = str_replace("]=>\n", '] = ', $c);
		$c = preg_replace('/= {2,}/', '= ', $c);
		$c = preg_replace("/\[\"(.*?)\"\] = /i", "[$1] = ", $c);
		$c = preg_replace('/  /', "    ", $c);
		$c = preg_replace("/\"\"(.*?)\"/i", "\"$1\"", $c);
		$c = preg_replace("/(int|float)\(([0-9\.]+)\)/i", "$1() <span class=\"number\">$2</span>", $c);

		// Syntax Highlighting of Strings. This seems cryptic, but it will also allow non-terminated strings to get parsed.
		$c = preg_replace("/(\[[\w ]+\] = string\([0-9]+\) )\"(.*?)/sim", "$1<span class=\"string\">\"", $c);
		$c = preg_replace("/(\"\n{1,})( {0,}\})/sim", "$1</span>$2", $c);
		$c = preg_replace("/(\"\n{1,})( {0,}\[)/sim", "$1</span>$2", $c);
		$c = preg_replace("/(string\([0-9]+\) )\"(.*?)\"\n/sim", "$1<span class=\"string\">\"$2\"</span>\n", $c);

		$regex = array(
			// Numberrs
			'numbers' => array('/(^|] = )(array|float|int|string|resource|object\(.*\)|\&amp;object\(.*\))\(([0-9\.]+)\)/i', '$1$2(<span class="number">$3</span>)'),
			// Keywords
			'null' => array('/(^|] = )(null)/i', '$1<span class="keyword">$2</span>'),
			'bool' => array('/(bool)\((true|false)\)/i', '$1(<span class="keyword">$2</span>)'),
			// Types
			'types' => array('/(of type )\((.*)\)/i', '$1(<span class="type">$2</span>)'),
			// Objects
			'object' => array('/(object|\&amp;object)\(([\w]+)\)/i', '$1(<span class="object">$2</span>)'),
			// Function
			'function' => array('/(^|] = )(array|string|int|float|bool|resource|object|\&amp;object)\(/i', '$1<span class="function">$2</span>('),
		);

		foreach ($regex as $x) {
			$c = preg_replace($x[0], $x[1], $c);
		}

		$style = '
/* outside div - it will float and match the screen */
.dumpr {
    margin: 2px;
    padding: 2px;
    background-color: #fbfbfb;
    float: left;
    text-align: left;
    clear: both;
}
/* font size and family */
.dumpr pre {
    color: #000000;
    font-size: 9pt;
    font-family: "Courier New",Courier,Monaco,monospace;
    margin: 0px;
    padding-top: 5px;
    padding-bottom: 7px;
    padding-left: 9px;
    padding-right: 9px;
}
/* inside div */
.dumpr div {
    background-color: #fcfcfc;
    border: 1px solid #d9d9d9;
    float: left;
    clear: both;
}
/* syntax highlighting */
.dumpr span.string {color: #c40000;}
.dumpr span.number {color: #ff0000;}
.dumpr span.keyword {color: #007200;}
.dumpr span.function {color: #0000c4;}
.dumpr span.object {color: #ac00ac;}
.dumpr span.type {color: #0072c4;}
';

		$style = preg_replace("/ {2,}/", "", $style);
		$style = preg_replace("/\t|\r\n|\r|\n/", "", $style);
		$style = preg_replace("/\/\*.*?\*\//i", '', $style);
		$style = str_replace('}', '} ', $style);
		$style = str_replace(' {', '{', $style);
		$style = trim($style);

		$c = trim($c);
		$c = preg_replace("/\n<\/span>/", "</span>\n", $c);

		if ($label == '') {
			$line1 = '';
		} else {
			$line1 = "<strong>$label</strong> \n";
		}

		$out = "\n<!-- Dumpr Begin -->\n" .
			"<style type=\"text/css\">" . $style . "</style>\n" .
			"<div class=\"dumpr\">
    <div><pre>$line1 $callingFile : $callingFileLine \n$c\n</pre></div></div><div style=\"clear:both;\">&nbsp;</div>" .
			"\n<!-- Dumpr End -->\n";
		if ($return) {
			return $out;
		} else {
			echo $out;
		}

		flush();
	}
}

if (! function_exists('formatNumber'))
{

	function formatNumber($value): string
	{
		return number_format($value, 0, ',', '.');
	}
}

if (! function_exists('formatCurrency'))
{
	function formatCurrency($value, $currencychar = '€'): string
	{
		return number_format($value, 0, ',', '.') . ' ' . $currencychar;
	}
}


if (! function_exists('getUrlParams'))
{
	function getUrlParams($url, $params = [], $overwrite_key = null, $overwrite_value = null ): string
	{

		$all = [];
		foreach ( $params as $key => $value )
			$all[$key] = $value;

		if( $overwrite_key )
			$all[$overwrite_key] =  $overwrite_value;

		return '?' . http_build_query($all);
	}
}

if (! function_exists('exception_handler'))
{
	function exception_handler(\Exception $e)
	{
		var_dump($e);
	}
}
