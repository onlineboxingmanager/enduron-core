<?php
#ä
namespace Enduron\Core\Query;

use AllowDynamicProperties;
use Enduron\Core\Cache\CacheManager;
use Enduron\Core\DBAL\SQLLimit;
use Enduron\Core\ORM\BaseDAO;
use Enduron\Core\ORM\BaseObject;
use Enduron\Core\Utilities\BaseList;
use Enduron\Core\Utilities\UUID;
use Enduron\Core\Query\Criteria;
use ReturnTypeWillChange;

#[AllowDynamicProperties]
abstract class BaseQuery
{

    protected array $criterias = [];
    protected string $tablename;
    protected string $modelname; // name of manager class => example: "PartnerManager"
    protected string $table_alias;
    protected array $joins = [];
    protected array $order = [];
    protected array $groupby;
    protected string $having;
    protected array $select = [];
    protected array $select_custom = [];
    protected int $offset = 0;
    protected int $limit = 30; // default

    public bool $test = false;

    protected bool $use_cache = false;

    protected array $params = [];
    protected string $jointype;
    protected ?string $joinname;

    public function getTablename(): string
    {
        return $this->tablename;
    }

    #[ReturnTypeWillChange]
    public function useCache(bool $boolean): self
    {
        $this->use_cache = $boolean;

        return $this;
    }

    #[ReturnTypeWillChange]
    public function limit(int $limit): self
    {
        return $this->setLimit($limit);
    }

    #[ReturnTypeWillChange]
    public function setLimit(int $limit): self
    {
        $this->limit = $limit;

        return $this;
    }

    #[ReturnTypeWillChange]
    public function setOffset(int $offset): self
    {
        $this->offset = $offset;

        return $this;
    }

    #[ReturnTypeWillChange]
    public function setTablename(string $string): self
    {
        $string = strtolower($string);
        $this->table_alias = substr(md5($string), 0, 12);
        $this->tablename = $string;

        return $this;
    }

    #[ReturnTypeWillChange]
    public function add(string $column, mixed $operator = null, mixed $expected_value = null, string $type = Criteria::LOGICAL_AND): self
    {
        // Default "="
        if( is_null($expected_value) AND !in_array($operator, [Criteria::ISNOTNULL, Criteria::ISNULL, Criteria::CUSTOM]) )
        {
            $expected_value = $operator;
            $operator = Criteria::EQUAL;
        }

	    // force uuid types
	    if( is_string($expected_value) AND UUID::isUuidString($expected_value) )
		    $expected_value = new UUID($expected_value);

        $myCriteria = $column;
        if ( !$column instanceof Criteria )
            $myCriteria = new Criteria($operator, $column, $expected_value);

        $myCriteria->setType($type);
        $this->criterias[] = $myCriteria;

        return $this;
    }

    #[ReturnTypeWillChange]
    public function whereRaw(string $column): self
    {
        return $this->add($column, Criteria::CUSTOM, null, Criteria::LOGICAL_AND);
    }

    #[ReturnTypeWillChange]
    public function orWhereRaw(string $column): self
    {
        return $this->add($column, Criteria::CUSTOM, null, Criteria::LOGICAL_OR);
    }

    #[ReturnTypeWillChange]
    public function where(string $column, mixed $operator, mixed $expected_value = null): self
    {
        return $this->add($column, $operator, $expected_value, Criteria::LOGICAL_AND);
    }

    #[ReturnTypeWillChange]
    public function active(): self
    {
        return $this->add($this->getTablename() . '.tstamp_deleted', Criteria::ISNULL);
    }

    #[ReturnTypeWillChange]
    public function deleted(): self
    {
        return $this->add($this->getTablename() . '.tstamp_deleted', Criteria::ISNOTNULL);
    }

    #[ReturnTypeWillChange]
    public function addOr(string $column, mixed $operator = null, mixed $expected_value = null): self
    {
        return $this->add($column, $operator, $expected_value, Criteria::LOGICAL_OR);
    }

    /*
    public function addXOr($column, $operator = null, $expected_value = null) {
        if(! $column instanceof Criteria )
            $myCriteria = new Criteria($operator, $column, $expected_value);
        else
            $myCriteria = $column;
        $myCriteria->setType(Criteria::LOGICAL_XOR);
        $this->criterias[] = $myCriteria;
    }*/

    #[ReturnTypeWillChange]
    public function addJoin(BaseQuery $myJoinQuery, string $condition, ?string $joinname = null): self
    {
        $myJoinQuery->jointype = $condition;
        $myJoinQuery->joinname = $joinname;
        $this->joins[] = $myJoinQuery;

        return $this;
    }

    #[ReturnTypeWillChange]
    public function orderby(string $column, string $sorttype = ''): self
    {
        $this->order[] = $column . ' ' . $sorttype;

        return $this;
    }

    public function addorder($column, $sorttype = ''): self
    {
        return $this->orderby($column, $sorttype);
    }

    #[ReturnTypeWillChange]
    public function groupBy(string $column): self
    {
        $this->groupby[] = $column;

        return $this;
    }

    #[ReturnTypeWillChange]
    public function addhaving(string $string)
    {
        return $this->having($string);
    }

    #[ReturnTypeWillChange]
    public function having(string $string): self
    {
        $this->having = $string;

        return $this;
    }

    #[ReturnTypeWillChange]
    public function clearSelects(): self
    {
        $this->select = [];

        return $this;
    }

    #[ReturnTypeWillChange]
    public function addSelect(string $column): self
    {
        $this->select[] = $column;

        return $this;
    }

    #[ReturnTypeWillChange]
    public function addSelectCustom(string $column, string $name): self
    {
        $this->select_custom[$name] = $column;

        return $this;
    }

    public function count(): int
    {
        return $this->build('count');
    }

    #[ReturnTypeWillChange]
    public function build(string $mode = ''): mixed
    {
        $is_count = ($mode == 'count');
        $is_showsql = ($mode == 'showsql');

        $models = [];
        $models["main"] = $this->modelname;
        $models["joins"] = [];
        $params = [];
        $this->params = [];

        // spalten von hauptklasse holen
        $select_rows = '';
        $cols = call_user_func(array('\Enduron\Models\\'.$this->modelname.'\\'.$this->modelname . 'DAO', 'getColumns'));
        if ( is_array($cols) AND !empty($cols) )
        {
            $select_rows = implode(', ', $cols);
        }

        // custom
        foreach ( $this->select_custom as $key => $value )
            $select_rows .= ", \n" . $value . ' as ' . $key;

        // JOINS
        $joinsql = '';
        foreach ( $this->joins as $jointable )
        {
            /**
             * @var BaseQuery $jointable
             */

            $models["joins"][] = $jointable->modelname;

            $tablename = $jointable->tablename;
            //$tablename = substr($jointable->tablename, strpos($jointable->tablename, '.') + 1);

            // spalten zum select hinzufügen
            $cols = call_user_func(array('\Enduron\Models\\'.$jointable->modelname.'\\'.$jointable->modelname . 'DAO', 'getColumns'));

            if ( $jointable->joinname AND is_array($cols) AND !empty($cols) )
            {
                foreach ( $cols as $key => $value )
                    $cols[$key] = str_replace(' as ', ' as ' . strtoupper($jointable->joinname) . '__', $value);
            }

            if ( is_array($cols) AND !empty($cols) )
            {
                $select_rows .= ", \n" . implode(', ', $cols);
            }

            if ( $jointable->joinname )
            {
                $select_rows = str_replace($tablename, $jointable->joinname, $select_rows);
            }

            // join typ
            $joinsql .= "\n" . ' ' . $jointable->jointype . ' ' . $jointable->tablename . ' ' . ($jointable->joinname ? $jointable->joinname : '') . ' ON 1 = 1';

            // bedingungen
            foreach ( $jointable->criterias as $key => $criteria )
            {
                $last = (isset($jointable->criterias[$key - 1]) ? $jointable->criterias[$key - 1] : null);
                $next = (isset($jointable->criterias[$key + 1]) ? $jointable->criterias[$key + 1] : null);

                switch ( $criteria->getType() )
                {
                    case Criteria::LOGICAL_AND:
                    case Criteria::LOGICAL_OR:
                        $joinsql .= ' ' . $criteria->getType() . ' ';
                        break;
                }

                if ( $next AND $criteria->getType() == Criteria::LOGICAL_AND AND $next->getType() == Criteria::LOGICAL_OR )
                {
                    $joinsql .= ' ( ';
                }

                $where = $criteria->getWhereClause();
                if ( $jointable->joinname )
                {
                    $where = str_replace($tablename, $jointable->joinname, $where);
                }
                $joinsql .= $where;

                //if( $criteria->getType() == Criteria::LOGICAL_OR AND $last->getType() == Criteria::LOGICAL_AND )
                if ( $criteria->getType() == Criteria::LOGICAL_OR AND (!$next OR $next->getType() != Criteria::LOGICAL_OR) )
                {
                    $joinsql .= ' ) ';
                }

                //$joinsql .= ' '.$criteria->getType().' ';
                //$joinsql .= $criteria->getWhereClause();

                if ( $criteria->getValue() == '?' )
                {
                    $params[] = $criteria->getParam();
                }
            }
        }

        // wenn groupby benutzt wird dürfen nur diese spalten benutzt werden
        //if( !empty($this->groupby) )
        //$select_rows = implode(',', $this->groupby);

        $sql = 'SELECT ' . $select_rows . "\n FROM " . $this->tablename;
        $sql .= $joinsql;

        $sql .= "\n" . ' WHERE 1 = 1' . "\n";
        // where bedingungen
        foreach ( $this->criterias as $key => $criteria )
        {
            //$last = (isset($this->criterias[$key - 1]) ? $this->criterias[$key - 1] : null);
            $next = (isset($this->criterias[$key + 1]) ? $this->criterias[$key + 1] : null);

            switch ( $criteria->getType() )
            {
                case Criteria::LOGICAL_AND:
                case Criteria::LOGICAL_OR:
                    $sql .= ' ' . $criteria->getType() . ' ';
                    break;
            }

            if ( $next AND $criteria->getType() == Criteria::LOGICAL_AND AND $next->getType() == Criteria::LOGICAL_OR )
            {
                $sql .= ' ( ';
            }

            $sql .= $criteria->getWhereClause();

            //if( $criteria->getType() == Criteria::LOGICAL_OR AND $last->getType() == Criteria::LOGICAL_AND )
            if ( $criteria->getType() == Criteria::LOGICAL_OR AND (!$next OR $next->getType() != Criteria::LOGICAL_OR) )
            {
                $sql .= ' ) ';
            }

            #if( !in_array($criteria->getOperator(), array(Criteria::ISNOTNULL, Criteria::ISNULL)) )
            if ( !is_null($criteria->getValue()) AND strlen($criteria->getValue()) > 0 AND !preg_match('/^(tbl_)?[a-z\d\_]+\.[a-z\d\_]+$/ui', $criteria->getValue()) AND !in_array($criteria->getOperator(), array(Criteria::ISNOTNULL, Criteria::ISNULL, Criteria::CUSTOM, Criteria::NOT_IN, Criteria::IN)) )
            {
                $params[] = $criteria->getParam();
            }
        }

        if ( !empty($this->groupby) )
        {
            $sql .= ' GROUP BY ' . implode(' ,', $this->groupby);
        }

        if ( !empty($this->having) )
        {
            $sql .= "\n" . ' HAVING ' . $this->having . " \n";
        }

        if ( !empty($this->order) )
        {
            $sql .= ' ORDER BY ' . implode(' ,', $this->order);
        }

        if ( $is_showsql )
        {
            return $sql;
            #dump($this->params);
            #echo "<hr />";
        }

        // wenn count gebraucht wird
        if ( $is_count )
        {
            $sql = 'SELECT count(*) cnt FROM (' . $sql . ') x';
        }

        // select verarbeiten und result an klassen übergeben

        // count ausgeben - limit/offset weglassen
        if ( !$this->use_cache AND $is_count )
        {
            if( $myResult = BaseDAO::genericQuery($sql, $params) AND $myResult AND $myResult->next() )
            {
                return $myResult->getInt('cnt');
            }
        }

        #############################################################
        # CACHING
        #############################################################
        // daten über aus cache auslesen
        $hash_key = 'DB_QUERY_' . md5($sql . implode('#', $params));
        if ( $this->use_cache === true AND !$is_count AND ($data = CacheManager::get($hash_key)) AND $data )
        {
            return unserialize($data);
        }
        #############################################################

        else
        {
            // richtig suchen
            #echo "<pre>";
            #var_dump($params);
            #echo "</pre>";

	        if( $_ENV['is_debug'] ?? false )
	            echo $sql . "\n\n";

	        $timer_start = microtime(true);
            $myResult = BaseDAO::genericQuery($sql, $params, new SQLLimit($this->limit, $this->offset));
	        @$_ENV['db_timer'] += microtime(true) - $timer_start;
            if ( !$myResult )
            {
                return false;
            }
        }

        $classname = '\Enduron\Models\\' . $this->modelname . '\\' . $this->modelname . 'List';
        $myList = new $classname();
        $get_references_from_database = !empty($this->joins);
        while ( $myResult->next() )
        {
            $myBaseObject = call_user_func_array(array('\Enduron\Models\\' . $this->modelname . '\\' . $this->modelname . 'DAO', 'get' . $this->modelname . 'FromResult'), array($myResult, $get_references_from_database));

            foreach ( $this->select_custom as $name => $column )
            {
                $myBaseObject->$name = $myResult->getString($name);
            }

            // subs
            if ( !empty($this->joins) )
            {
                foreach ( $this->joins as $join )
                {
                    $classname = $join->modelname;
                    $customname = $join->joinname ? strtoupper($join->joinname) . '__' : '';
                    $customname = str_replace(['REF_', 'SUB_'], '', $customname);

                    try
                    {
                        $mySubObject = call_user_func_array(array( '\Enduron\Models\\' . $classname . '\\' . $classname . 'DAO', 'get' . $classname . 'FromResult'), array($myResult, false, $customname));
                    }
                    catch (\Exception $e)
                    {
                        var_dump($e);
                        throw $e;
                    }

                    if ( $join->joinname )
                    {
                        $classname = $join->joinname;
                    }

                    $function = 'set' . $classname;

                    if ( method_exists($myBaseObject, $function) )
                    {
                        if( $mySubObject )
                            $myBaseObject->$function($mySubObject);
                    }
                    else
                    {
                        $myBaseObject->$classname = $mySubObject;
                    }
                }
            }

            $myList->add($myBaseObject);
        }

        $myResult->clear();

        #############################################################
        # CACHING
        #############################################################
        if ( $this->use_cache === true )
        {
            CacheManager::set($hash_key, serialize($myList));
        }

        #############################################################

        return $myList;
    }

    public function showSQL(): string
    {
        return $this->toSql();
    }

    public function toSql(): string
    {
        return $this->build('showsql');
    }

    #[ReturnTypeWillChange]
    public function findOne(): ?BaseObject
    {
        $this->setLimit(1);
        $myList = $this->build();

        return $myList->valid() ? $myList->current() : null;
    }

    #[ReturnTypeWillChange]
    public function first(): ?BaseObject
    {
        return $this->findOne();
    }

    #[ReturnTypeWillChange]
    public function get(?SQLLimit $myLimit = null): BaseList
    {
	    return $this->find($myLimit);
    }

	#[ReturnTypeWillChange]
	public function find(?SQLLimit $myLimit = null): BaseList
	{
		if( $myLimit )
		{
			$this->setLimit($myLimit->getLimit());
			$this->setOffset($myLimit->getOffset());
		}
		return $this->build();
	}

}