<?php
#ä
namespace Enduron\Core;

class CmdKernel
{

	protected array $commands = [
		'help' => "help\t\t\t\t\t\t\tShow this help page",
		'list' => "list\t\t\t\t\t\t\tShow all available commands",
		'cache:clear' => "clear\t\t\t\t\t\tClear caches and dump composer autoloader",
		'setup:demo' => "setupDemo\t\t\t\t\t\tSetup demo project and database tables",
		'ormcompiler:run' => "ormcompiler\t\t\t\t\tAuto-generate Models, DAO-Classes & JS Typescript files",
		'dumpautoloader' => "dumpautoloader\t\t\t\t\tDump composer autoloader"
	];

	public function __construct()
	{
		echo $this->colorize("###################################\n", 'green');
		echo $this->colorize("ENDURON FRAMEWORK CLI\n", 'green');
		echo $this->colorize("###################################\n", 'green');
	}

	public function run(): int
	{
		$this->handle($_SERVER['argv']);

		return 0; // shell status
	}

	public function handle($argv)
	{
		$commandName = $argv[1] ?? 'help';

		if( $commandName === 'help' )
			return $this->showHelp();

		if ( isset($this->commands[$commandName]) )
		{
			$commandClass = substr($this->commands[$commandName], 0, strpos($this->commands[$commandName], "\t") );
			if( method_exists($this, $commandClass) )
				return $this->$commandClass($argv);
			else
				exit("method $commandClass not exists\n");

			//$command = new $commandClass();
			//$command->execute(array_slice($argv, 2));
		} else {
			echo $this->colorize("Unbekannter Befehl: $commandName\n\n", 'red');
			$this->showHelp();
		}
	}

	protected function showHelp()
	{
		echo $this->colorize("Verfügbare Befehle:\n", 'green');
		foreach ($this->commands as $name => $class)
		{
			$description = substr($class, strpos($class, "\t"));
			echo $this->colorize("  $name $description\n", 'yellow');
		}
	}

	protected function colorize($text, $color)
	{
		$colors = [
			'red' => "\033[31m",
			'green' => "\033[32m",
			'yellow' => "\033[33m",
			'reset' => "\033[0m",
		];

		$colorCode = $colors[$color] ?? $colors['reset'];
		return $colorCode . $text . $colors['reset'];
	}

	protected function clear()
	{
		// clear cache and view cache here
		// ...

		if( function_exists('apcu_clear_cache') AND apcu_clear_cache() )
			echo $this->colorize("Cache cleared\n", 'green');

		$this->dumpautoloader();
	}

	protected function dumpautoloader()
	{
		echo shell_exec('cd ' . ROOT_PATH .  ' && composer dumpautoload');
	}

	protected function ormcompiler()
	{
		echo shell_exec('cd ' . ROOT_PATH .  '/vendor/enduron/ormcompiler && php run.php');
	}

	protected function setupDemo()
	{

	}

}